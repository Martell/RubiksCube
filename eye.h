#ifndef EYE_H
#define EYE_H
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
using namespace cv;
char * getColor(Mat src);
#endif // EYE_H
