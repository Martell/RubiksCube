#include "eye.h"

char * getColor(Mat src) {
    char colors[9];
    Mat gaussian, gray, grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y, abs_dst;
    resize(src, src, Size(src.size().width/10, src.size().height/10));
    imshow("src", src);
    //GaussianBlur(src, gaussian, Size(5, 5), 0, 0);
    //imshow("gaussian", gaussian);
    cvtColor(src, gray, COLOR_BGR2GRAY);
    Sobel(gray, grad_x, gray.depth(), 2, 0, 3, 2, 2, BORDER_DEFAULT);
    convertScaleAbs(grad_x, abs_grad_x);
    //imshow("x", abs_grad_x);
    Sobel(gray, grad_y, gray.depth(), 0, 2, 3, 2, 2, BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);
    //imshow("y", abs_grad_y);
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, abs_dst);
    imshow("x+y", abs_dst);
    threshold(abs_dst, abs_dst, 0, 255, CV_THRESH_OTSU+CV_THRESH_BINARY);
    imshow("threshold", abs_dst);

    Mat element = getStructuringElement(MORPH_RECT, Size(5, 5));
    Mat closed;
    dilate(abs_dst, closed, element);
    erode(closed, closed, element);
    imshow("closed", closed);

    waitKey(0);
    return colors;
}
