#ifndef CUBE_H
#define CUBE_H

struct Piece{
    char type;
    char color[3];
    char direction[3];
};

enum { // type of Pieces
    PCENTER = 0,
    PMIDDLE = 1,
    PHORN = 2,
    PSHAFT = 3,
};
enum { // Colors of Mofang
    CWHITE = 0,
    CRED = 1,
    CGREEN = 2,
    CORANGE = 3,
    CBLUE = 4,
    CYELLOW = 5,
};
enum { // Index of sides
    SUP = 0,
    SDOWN = 1,
    SFRONT = 2,
    SBACK = 3,
    SLEFT = 4,
    SRIGHT = 5
};
enum { // Parameter of Turn()
    TURN_UP = 0,
    TURN_DOWN = 1,
    TURN_LEFE = 2,
    TURN_RIGHT = 3
};
enum { // Angle of Move()
    ANGLE_RIGHT = 0,
    ANGLE_STRAIGHT = 1,
    ANGLE_NEGATIVE_RIGHT = 2,
};

const char CENTERS[] = { 4, 10, 12, 14, 16, 22 };
const char MIDDLES[] = { 1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25 };
const char HORNS[] = { 0, 2, 6, 8, 18, 20, 24, 26 };
const char SHAFT[] = { 13 };
const char FACE_INDEXS[6][9] = { { 0, 1, 2, 5, 8, 7, 6, 3, 4 }, { 6, 7, 8, 16, 25, 24, 23, 14, 15 }, { 8, 5, 2, 11, 20, 23, 26, 17, 14 },
                           { 2, 1, 0, 9, 17, 18, 19, 11, 10}, { 0, 3, 6, 15, 24, 21, 18, 9, 12 }, { 20, 19, 18, 21, 24, 25, 26, 23, 22 } };


#endif // CUBE_H
