#ifndef MOFANG_H
#define MOFANG_H

struct Piece{
    char type;
    char color[3];
    char direction[3];
};

enum {
    // type of Pieces
    PCENTER = 0,
    PMIDDLE = 1,
    PPOINTEDNESS = 2,
};
enum {
    // Colors of Mofang
    CWHITE = 0,
    CYELLOW = 1,
    CRED = 2,
    CORANGE = 3,
    CGREEN = 4,
    CBLUE = 5
};
enum {
    // Index of sides
    SUP = 0,
    SDOWN = 1,
    SFRONT = 2,
    SBACK = 3,
    SLEFT = 4,
    SRIGHT = 5
};
enum {
    // Parameter of Turn()
    TURN_UP = 0,
    TURN_DOWN = 1,
    TURN_LEFE = 2,
    TURN_RIGHT = 3
};
enum {
    // Angle of Move()
    ANGLE_RIGHT = 0,
    ANGLE_STRAIGHT = 1,
    ANGLE_NEGATIVE_RIGHT = 2,
};
class Mofang
{
    public:
        Mofang();
        virtual ~Mofang();
        void Turn(char direction);
        void Move(char angle);
    protected:

    private:
        Piece pieces[26];
        char positions[26]; // [3][3][3] : z y x
        char sides[6];
};

#endif // MOFANG_H
